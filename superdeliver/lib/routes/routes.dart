import 'package:flutter/material.dart';
import 'package:superdeliver/screens/superDeliver/confirmation_page.dart';
import 'package:superdeliver/screens/superDeliver/login.dart';
import 'package:superdeliver/screens/superDeliver/menu_bon.dart';
import 'package:superdeliver/screens/superDeliver/order_routes.dart';
import 'package:superdeliver/screens/superDeliver/scan_screen.dart';
import 'package:superdeliver/screens/superDeliver/orders.dart';
import 'package:superdeliver/main.dart';
import 'package:superdeliver/screens/superDeliver/signature.dart';
import 'package:superdeliver/screens/superPicker/pickProduct.dart';
import 'package:superdeliver/screens/superPicker/login.dart';
import 'package:superdeliver/screens/superPicker/productListByLvl.dart';
import 'package:superdeliver/screens/superPicker/scan_location.dart';
import 'package:superdeliver/screens/superPicker/setProduct.dart';

Map<String, WidgetBuilder> getAppRoutes() {
  return {
    '/home': (context) => const HomeScreen(),
    '/superdeliverLogin': (context) => const LoginScreen(),
    '/superLocatorLogin': (context) => const LoginScreenLocator(),
    '/scan': (context) => const ScanScreen(),
    '/scanLocation': (context) => const ScanScreenLocator(),
    '/orderList': (context) => const OrderListScreen(),
    '/orderRoutes': (context) => const OrderRoute(),
    '/menuBon': (context) => const MenuBon(),
    '/signature': (context) => const SignatureView(),
    '/confirmation': (context) => const ConfirmationPage(),
    '/getProduct': (context) => const GetProduct(),
    '/setProduct': (context) => const SetProduct(),
    '/level': (context) => const ProductListByLvl(),
  };
}

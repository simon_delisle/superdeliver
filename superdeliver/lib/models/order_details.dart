// ignore_for_file: file_names, non_constant_identifier_names
import 'dart:convert';

class Order {
  late final String order_number;
  final int store;
  final String customer;
  final String client_name;
  final String phone_number;
  final String address;
  final int driver_id;
  List<Item> orderInfo;
  bool is_arrived;
  bool is_delivered;
  final double latitude;
  final double longitude;
  final int order_index;
  final String route;
  bool route_started;
  final String? received_by;

  Order({
    required this.order_number,
    required this.store,
    required this.customer,
    required this.client_name,
    required this.phone_number,
    required this.address,
    required this.driver_id,
    required this.orderInfo,
    required this.is_arrived,
    required this.is_delivered,
    required this.longitude,
    required this.latitude,
    required this.order_index,
    required this.route,
    required this.route_started,
    required this.received_by,
  });

  factory Order.fromJson(Map<String, dynamic> json) {
    var orderInfoData = json['order_info'];
    List<Item> parsedOrderInfo = [];

    if (orderInfoData is String) {
      orderInfoData = jsonDecode(orderInfoData);
    }

    if (orderInfoData is List) {
      parsedOrderInfo = orderInfoData
          .map<Item>((json) => Item.fromJson(json as Map<String, dynamic>))
          .toList();
    }

    return Order(
      order_number: json['order_number'] as String,
      store: json['store'] as int,
      customer: json['customer'] as String,
      client_name: json['client_name'] as String,
      phone_number: json['phone_number'] as String,
      address: json['address'] as String,
      driver_id: json['driver_id'] as int,
      orderInfo: parsedOrderInfo,
      is_arrived: json['is_arrived'] == 1 || json['route_started'] == true,
      is_delivered: json['is_delivered'] == 1 || json['route_started'] == true,
      latitude: json['latitude'] as double,
      longitude: json['longitude'] as double,
      order_index: json['order_index'] as int,
      route: json['route'] as String,
      route_started:
          json['route_started'] == 1 || json['route_started'] == true,
      received_by: json['received_by'] as String?,
    );
  }
}

class Item {
  final String part_number;
  final String description;
  final int units;
  int num_scanned;
  int confirmed_scanned;

  Item({
    required this.part_number,
    required this.description,
    required this.units,
    required this.num_scanned,
    required this.confirmed_scanned,
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      part_number: json['item'] as String,
      description: json['description'] as String,
      units: json['units'] as int,
      num_scanned: json['num_scanned'] as int,
      confirmed_scanned: json['confirmed_scanned'] as int,
    );
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// Cookie lifespan in hours
const int cookieLifespanHours = 12;

/// Key for storing locator cookie
const String locatorCookieKey = 'locator_cookie';

/// Key for storing driver cookie
const String driverCookieKey = 'driver_cookie';

/// Key for storing picker cookie
const String pickerCookieKey = 'picker_cookie';

/// Stores a cookie securely with a specified lifespan.
///
/// @param token The token to store in the cookie
/// @param cookieKey The key to store the cookie under
///
/// Example:
/// ```dart
/// await storeCookie('my_token', locatorCookieKey);
/// ```
Future<void> storeCookie(String token, String cookieKey) async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  final DateTime expiryTime =
      DateTime.now().add(const Duration(hours: cookieLifespanHours));
  final Map<String, dynamic> cookieData = {
    'token': token,
    'expiryTime': expiryTime.toIso8601String(),
  };
  await secureStorage.write(key: cookieKey, value: jsonEncode(cookieData));
}

/// Retrieves a stored cookie, checking for expiration.
///
/// @param cookieKey The key to retrieve the cookie from
///
/// @return The stored cookie data, or null if expired or not found
///
/// Example:
/// ```dart
/// final cookieData = await retrieveCookie(locatorCookieKey);
/// if (cookieData != null) {
///   print(cookieData['token']);
/// }
/// ```
Future<Map<String, dynamic>?> retrieveCookie(String cookieKey) async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  final String? cookieString = await secureStorage.read(key: cookieKey);
  if (cookieString == null) return null;
  final Map<String, dynamic> cookieData = jsonDecode(cookieString);
  final DateTime expiryTime = DateTime.parse(cookieData['expiryTime']);
  if (DateTime.now().isAfter(expiryTime)) {
    await secureStorage.delete(key: cookieKey);
    return null;
  }
  return cookieData;
}

/// Retrieves the stored signature name.
///
/// @return The stored signature name, or an empty string if not found
///
/// Example:
/// ```dart
/// final signatureName = await getSignatureName();
/// print(signatureName);
/// ```
Future<String> getSignatureName() async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  return await secureStorage.read(key: 'signatureName') ?? '';
}

/// Stores a token securely.
///
/// @param token The token to store
///
/// Example:
/// ```dart
/// await storeTokenSecurely('my_token');
/// ```
Future<void> storeTokenSecurely(String token) async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  await secureStorage.write(key: 'userToken', value: token);
}

/// Retrieves the stored token securely.
///
/// @return The stored token, or an empty string if not found
///
/// Example:
/// ```dart
/// final token = await retrieveTokenSecurely();
/// print(token);
/// ```
Future<String> retrieveTokenSecurely() async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  return await secureStorage.read(key: 'userToken') ?? '';
}

/// Logs out the user, deleting stored tokens and cookies.
///
/// @param context The BuildContext to navigate from
///
/// Example:
/// ```dart
/// await logout(context);
/// ```
Future<void> logout(BuildContext context) async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();

  // Delete the user token
  await secureStorage.delete(key: 'userToken');

  // Delete the locator-specific cookie
  await secureStorage.delete(key: locatorCookieKey);

  // Navigate to the home screen and remove all previous routes
  Navigator.of(context)
      .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
}

/// Retrieves the stored SDK keys.
///
/// @return A map containing the SDK key ID and key, or empty strings if not found
///
/// Example:
/// ```dart
/// final sdkKeys = await retrieveSdkKeys();
/// print(sdkKeys);
/// ```
Future<Map<String, dynamic>> retrieveSdkKeys() async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  String? sdkKeyId = await secureStorage.read(key: 'sdk_key_id');
  String? sdkKey = await secureStorage.read(key: 'sdk_key');

  return {
    "sdk_key_id": sdkKeyId ?? '',
    "sdk_key": sdkKey ?? '',
  };
}

Future<String> retrieveDriverId() async {
  // Replace this with your actual logic to retrieve the driver ID
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  return await secureStorage.read(key: 'driverId') ?? '';
}

Future<String> retrieveStoreId() async {
  // Replace this with your actual logic to retrieve the driver ID
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();
  print('fetch store id');
  return await secureStorage.read(key: 'storeId') ?? '';
}

Future<void> locatorLogout(BuildContext context) async {
  const FlutterSecureStorage secureStorage = FlutterSecureStorage();

  // Delete the user token
  await secureStorage.delete(key: 'userToken');

  // Delete the locator-specific cookie
  await secureStorage.delete(key: locatorCookieKey);

  // Navigate to the home screen and remove all previous routes
  Navigator.of(context)
      .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:here_sdk/consent.dart';
import 'package:here_sdk/core.dart';
import 'package:here_sdk/core.engine.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:superdeliver/environment/environment.dart';
import 'package:superdeliver/providers/locator_provider.dart';
import 'package:superdeliver/providers/order_provider.dart';
import 'package:superdeliver/routes/routes.dart';
import 'package:superdeliver/screens/superDeliver/orders.dart';
import 'package:superdeliver/screens/superPicker/scan_location.dart';
import 'package:superdeliver/stores/store.dart';
import 'package:superdeliver/variables/svg.dart';
import 'package:wakelock/wakelock.dart';

/// A global key for the scaffold messenger state.
final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
    GlobalKey<ScaffoldMessengerState>();

/// A global key for the navigator state.
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

/// The main function of the application.
///
/// This function initializes the environment, retrieves tokens and SDK keys,
/// and sets up the initial route based on the token and cookie data.
Future<void> main() async {
  const environment = String.fromEnvironment('ENV', defaultValue: 'dev');
  Environment env = (environment == "prod")
      ? Environment.production()
      : Environment.development();

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  // Retrieve token and SDK keys securely
  final token = await retrieveTokenSecurely();
  final sdkKeys = await retrieveSdkKeys();
  final sdkKeyId = sdkKeys["sdk_key_id"];
  final sdkKey = sdkKeys["sdk_key"];

  // Retrieve cookie data
  final locatorCookie = await retrieveCookie(locatorCookieKey);
  final driverCookie = await retrieveCookie(driverCookieKey);
  final customerCookie = await retrieveCookie(pickerCookieKey);

  // Initialize SDK and set initial route based on the token and cookie
  String initialRoute = '/home';
  if (token.isNotEmpty && sdkKeyId != null && sdkKey.isNotEmpty) {
    SdkContext.init(IsolateOrigin.main);
    SDKOptions sdkOptions = SDKOptions.withAccessKeySecretAndCachePath(
      sdkKeyId,
      sdkKey,
      "", // Assuming the third parameter is a string for cache path
    );
    await SDKNativeEngine.makeSharedInstance(sdkOptions);

    if (locatorCookie != null) {
      initialRoute = '/scanLocation';
    } else if (driverCookie != null) {
      initialRoute = '/orderList';
    } else if (customerCookie != null) {
      // Currently, nothing to do if the customer cookie is present
      initialRoute = '/home';
    }
  }
  runApp(MyApp(
    env: env,
    initialRoute: initialRoute,
  ));
  Wakelock.enable();
}

/// The main application widget.
///
/// This widget provides the environment, initial route, and current driver ID
/// to its child widgets.
class MyApp extends StatelessWidget {
  final Environment env;
  final String initialRoute;

  const MyApp({
    super.key,
    required this.env,
    required this.initialRoute,
  });

  @override
  Widget build(BuildContext context) {
    return Provider<Environment>.value(
      value: env,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (context) => OrderProvider(env.apiUrl)),
          ChangeNotifierProvider(
              create: (context) => LocatorProvider(env.apiUrl)),
        ],
        child: MaterialApp(
          scaffoldMessengerKey: scaffoldMessengerKey,
          debugShowCheckedModeBanner: false,
          navigatorKey: navigatorKey,
          supportedLocales: HereSdkConsentLocalizations.supportedLocales,
          localizationsDelegates:
              HereSdkConsentLocalizations.localizationsDelegates,
          home: Builder(
            builder: (context) {
              ScreenUtil.init(
                context,
                designSize: const Size(360, 690),
                minTextAdapt: true,
              );
              switch (initialRoute) {
                case '/scanLocation':
                  return const ScanScreenLocator();
                case '/orderList':
                  return const OrderListScreen();
                case '/home':
                default:
                  return const HomeScreen();
              }
            },
          ),
          initialRoute: initialRoute,
          routes: getAppRoutes(),
        ),
      ),
    );
  }
}

/// The home screen widget.
///
/// This widget displays a background image and three SVG icons for
/// SuperDeliver,SuperPicker, and SuperLocator.
class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background_hp.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/superdeliverLogin');
                  },
                  child: SvgPicture.string(
                    svgDeliver,
                    width: 120,
                    height: 120,
                  ), // SuperDeliver
                ),
                const SizedBox(height: 30),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/superLocatorLogin');
                  },
                  child: SvgPicture.string(
                    svgPicker,
                    width: 120,
                    height: 120,
                  ), // SuperLocator
                ),
                const SizedBox(height: 30),
                GestureDetector(
                  onTap: () {
                    // Navigator.pushNamed(context, '/superLocatorLogin');
                  },
                  child: SvgPicture.string(
                    svgTransfer,
                    width: 120,
                    height: 120,
                  ), // SuperLocator
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            child: Center(
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: const Text(
                  'version 1.2.3',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

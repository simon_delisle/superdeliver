class Environment {
  final String apiUrl;
  final String apiKey;
  final bool logEnabled;
  bool permissionShown = false;

  Environment(
      {required this.apiUrl, required this.apiKey, required this.logEnabled});

  bool isPermited() {
    return permissionShown;
  }

  void setToPermited() {
    permissionShown = true;
  }

  // Factory constructors for different environments
  factory Environment.development() {
    return Environment(
      apiUrl: 'http://192.168.1.170:8000',
      // apiUrl: 'http://192.168.1.128:8000',
      apiKey: 'dev-key',
      logEnabled: true,
    );
  }

  // Factory constructors for different environments
  // factory Environment.development() {
  //   return Environment(
  //     apiUrl: 'https://deliver-prod.pasuper.xyz',
  //     apiKey: 'dev-key',
  //     logEnabled: true,
  //   );
  // }

  factory Environment.production() {
    return Environment(
      apiUrl: 'https://deliver-dev.pasuper.xyz',
      apiKey: 'prod-key',
      logEnabled: false,
    );
  }
}

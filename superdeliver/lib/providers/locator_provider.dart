import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:superdeliver/stores/store.dart';

class LocatorProvider with ChangeNotifier {
  String apiUrl = '';
  String? currentStoreId; // Make it nullable
  String? highlightedItemId;
  Map<String, List<Map<String, dynamic>>> ordersByOrderNumber = {};

  // Getter for storeId
  String? get storeId => currentStoreId;

  // Constructor that initializes the API URL and starts the initialization process
  LocatorProvider(String apiUrlParam) {
    apiUrl = apiUrlParam;
    print('LocatorProvider constructor called');
    initialize();
  }

  // Initialization method to retrieve the storeId from secure storage
  Future<void> initialize() async {
    try {
      currentStoreId = await retrieveStoreId();
      print('retrieve currentStoreId');
      if (currentStoreId != null) {
        notifyListeners();
      } else {
        _handleError(Exception('Store ID is empty'), 'Initialization Error');
      }
    } catch (e) {
      _handleError(e, 'Error during Initialization');
    }
  }

  // Method to fetch picking orders by level
  Future<void> fetchPickingOrdersByLevel(int level) async {
    if (currentStoreId == null || currentStoreId!.isEmpty) {
      _handleError(Exception('No Store ID'), 'Store ID is null or empty');
      return;
    }

    final url = Uri.parse('$apiUrl/picker/get_picking_orders');

    try {
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({'store': currentStoreId, 'level': level.toString()}),
      );

      if (response.statusCode == 200) {
        final List<dynamic> data = json.decode(response.body);

        // Clear the existing ordersByOrderNumber map
        ordersByOrderNumber.clear();

        // Separate the data by order_number
        for (var item in data) {
          String orderNumber = item['order_number'];
          if (ordersByOrderNumber.containsKey(orderNumber)) {
            ordersByOrderNumber[orderNumber]!.add(item);
          } else {
            ordersByOrderNumber[orderNumber] = [item];
          }
        }
        notifyListeners();
      } else {
        _handleError(Exception('Failed request'), 'Failed to fetch route');
      }
    } catch (e) {
      _handleError(e, 'Error fetching current route');
    }
  }

  Future<void> toggleItemReserved(
    String orderNumber,
    Map<String, dynamic> item,
  ) async {
    try {
      // Reserve the new item
      final response = await http.post(
        Uri.parse('$apiUrl/picker/set_to_reserved'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'id': item['id'],
          'order_number': orderNumber,
          'loc': item['loc'],
        }),
      );

      if (response.statusCode == 200) {
        highlightedItemId = item['id']; // Update the highlighted item
        notifyListeners(); // Notify listeners to update the UI
      } else {
        print('Failed to reserve item');
      }
    } catch (e) {
      print('Error toggling reservation: $e');
    }
  }

  Future<void> setItemUnreserved(
    int itemId,
  ) async {
    try {
      final response = await http.post(
        Uri.parse('$apiUrl/picker/unreserved'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'id': itemId,
        }),
      );

      if (response.statusCode == 200) {
        if (highlightedItemId == itemId) {
          highlightedItemId =
              null; // Clear the highlighted item if it's the same
          notifyListeners(); // Notify listeners to update the UI
        }
        print('Item $itemId unreserved');
      } else {
        print('Failed to unreserve item $itemId');
      }
    } catch (e) {
      print('Error unreserving item $itemId: $e');
    }
  }

  // Error handler for logging errors
  void _handleError(Object error, String message) {
    if (kDebugMode) {
      print('$message: $error');
    }
  }
}

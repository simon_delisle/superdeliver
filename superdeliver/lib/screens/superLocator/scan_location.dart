import 'package:flutter/material.dart';
import 'package:superdeliver/providers/locator_provider.dart';
import 'package:superdeliver/variables/colors.dart';

/// A stateful widget that handles the scan screen functionality.
class ScanScreenLocator extends StatefulWidget {
  /// Creates a new instance of [ScanScreenLocator].
  const ScanScreenLocator({super.key});

  @override
  _ScanScreenState createState() => _ScanScreenState();
}

/// The state class for [ScanScreenLocator].
class _ScanScreenState extends State<ScanScreenLocator> {
  /// Precaches the images used in the scan screen.
  void precacheImages() {
    precacheImage(const AssetImage("assets/images/background_hp.png"), context);
    precacheImage(const AssetImage("assets/images/barcode_image.png"), context);
  }

  /// Shows a snack bar with the given message.
  ///
  /// Example:
  /// ```dart
  /// showSnackBar('Barcode scanned successfully!');
  /// ```
  void showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(
        message,
        textAlign: TextAlign.center,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
      ),
      backgroundColor: Colors.red,
      duration: const Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      margin: EdgeInsets.only(
          bottom: MediaQuery.of(context).size.height / 2 - 24,
          left: 12,
          right: 12),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Stack(
        children: <Widget>[
          BackgroundImage(),
          BarcodeImage(),
          ScanPrompt(),
          LogoutButton(),
        ],
      ),
    );
  }
}

/// A widget that displays the background image.
class BackgroundImage extends StatelessWidget {
  /// Creates a new instance of [BackgroundImage].
  const BackgroundImage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Positioned.fill(
      child: DecoratedBox(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background_hp.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}

/// A widget that displays the barcode image.
class BarcodeImage extends StatelessWidget {
  /// Creates a new instance of [BarcodeImage].
  const BarcodeImage({super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: const EdgeInsets.only(top: 50),
        child: Image.asset("assets/images/barcode_image.png"),
      ),
    );
  }
}

class ScanPrompt extends StatelessWidget {
  /// Creates a new instance of [ScanPrompt].
  const ScanPrompt({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/setProduct');
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.blue, // Background color
              minimumSize: const Size(250, 60), // Button size
            ),
            child: const Text(
              'Placer un produit',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
          const SizedBox(height: 50), // Add some space between the buttons
          ElevatedButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/getProduct');
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red, // Background color
              minimumSize: const Size(250, 60), // Button size
            ),
            child: const Text(
              'Sortir un produit',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// A logout button that displays a confirmation dialog when pressed.
///
/// This button is positioned at the bottom right corner of the screen and
/// has a red background color with white text. When pressed, it shows a
/// confirmation dialog to confirm the user's intention to log out.
///
/// Example:
/// ```dart
/// class MyHomePage extends StatelessWidget {
///   @override
///   Widget build(BuildContext context) {
///     return Scaffold(
///       body: Center(
///         child: LogoutButton(),
///       ),
///     );
///   }
/// }
/// ```

class LogoutButton extends StatelessWidget {
  const LogoutButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 20,
      right: 20,
      child: ElevatedButton(
        onPressed: () => showLogoutConfirmation(context),
        style: ElevatedButton.styleFrom(
          backgroundColor: superRed,
          foregroundColor: Colors.white,
        ),
        child: const Text('Déconnexion'),
      ),
    );
  }
}

Future<void> showLogoutConfirmation(BuildContext context) async {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Déconnexion'),
        content: const Text('Êtes-vous sûr de vouloir vous déconnecter?'),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(); // Close the dialog
            },
            child: const Text('Annuler'),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(); // Close the dialog
              logout(context); // Call the logout function
            },
            child: const Text('Déconnexion'),
          ),
        ],
      );
    },
  );
}

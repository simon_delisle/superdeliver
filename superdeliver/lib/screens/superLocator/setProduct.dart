import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:superdeliver/variables/colors.dart';
import 'package:superdeliver/variables/svg.dart';

class SetProduct extends StatefulWidget {
  const SetProduct({super.key});

  @override
  _SetProductState createState() => _SetProductState();
}

/// A widget that displays the background image.
class BackgroundImage extends StatelessWidget {
  /// Creates a new instance of [BackgroundImage].
  const BackgroundImage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Positioned.fill(
      child: DecoratedBox(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background_hp.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}

class _SetProductState extends State<SetProduct> {
  /// A method channel to communicate with the DataWedge plugin.
  static const MethodChannel channel = MethodChannel('datawedge');

  /// The scanned barcode value.
  String scannedBarcode = '';

  @override
  void initState() {
    super.initState();
    initializeDataWedge();
    WidgetsBinding.instance.addPostFrameCallback((_) => precacheImages());
  }

  @override
  void dispose() {
    channel.invokeMethod('stopScan');
    super.dispose();
  }

  /// Initializes the DataWedge plugin and sets up the method call handler.
  Future<void> initializeDataWedge() async {
    try {
      await channel.invokeMethod('startScan');
      channel.setMethodCallHandler((MethodCall call) async {
        if (call.method == 'barcodeScanned') {
          //  logic here
        }
      });
    } on PlatformException catch (e) {
      if (kDebugMode) {
        print("Failed to initialize scanner: '${e.message}'.");
      }
    }
  }

  /// Preloads the images to improve performance.
  Future<void> precacheImages() async {
    await precacheImage(
      const AssetImage('assets/images/background_hp.png'),
      context,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            const BackgroundImage(),
            Positioned(
              bottom: 16.0,
              left: 16.0,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/scanLocation');
                },
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(superRed),
                ),
                child: ColorFiltered(
                  colorFilter:
                      const ColorFilter.mode(Colors.white, BlendMode.srcIn),
                  child: SvgPicture.string(backIconString),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

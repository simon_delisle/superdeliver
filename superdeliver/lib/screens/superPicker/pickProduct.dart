import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:superdeliver/providers/locator_provider.dart';
import 'package:superdeliver/variables/svg.dart';

class GetProduct extends StatefulWidget {
  const GetProduct({super.key});

  @override
  _GetProductState createState() => _GetProductState();
}

class _GetProductState extends State<GetProduct> {
  String selectedValue = 'Bas'; // Default selected value

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => precacheImages());
  }

  Future<void> precacheImages() async {
    await precacheImage(
      const AssetImage('assets/images/background_hp-red.png'),
      context,
    );
  }

  int _getLevelFromDropdown(String value) {
    switch (value) {
      case 'Bas':
        return 1;
      case 'Mezzanine':
        return 2;
      case 'Haut':
        return 3;
      default:
        return 1; // Fallback to 'Bas' if somehow the value is invalid
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            const BackgroundImage(),
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.string(
                    svgPicker,
                    width: 120,
                    height: 120,
                  ),
                  const SizedBox(height: 16.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10.0),
                    child: Container(
                      padding: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(9.0),
                      ),
                      child: DropdownButton<String>(
                        value: selectedValue,
                        items: <String>['Bas', 'Mezzanine', 'Haut']
                            .map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            selectedValue = newValue!;
                          });
                        },
                        dropdownColor: Colors.white,
                        isExpanded: true,
                        underline: const SizedBox.shrink(),
                      ),
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  ElevatedButton(
                    onPressed: () async {
                      final level = _getLevelFromDropdown(selectedValue);
                      Provider.of<LocatorProvider>(context, listen: false)
                          .fetchPickingOrdersByLevel(level);
                      await Navigator.pushReplacementNamed(context, '/level');
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      minimumSize: const Size(150, 60),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(9),
                      ),
                    ),
                    child: const Text(
                      'Prêt à picker',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 16.0,
              left: 16.0,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/scanLocation');
                },
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(
                      const Color.fromARGB(255, 66, 59, 59)),
                ),
                child: ColorFiltered(
                  colorFilter:
                      const ColorFilter.mode(Colors.white, BlendMode.srcIn),
                  child: SvgPicture.string(backIconString),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BackgroundImage extends StatelessWidget {
  const BackgroundImage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Positioned.fill(
      child: DecoratedBox(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background_hp-red.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}

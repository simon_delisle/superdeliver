import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:superdeliver/providers/locator_provider.dart';
import 'package:superdeliver/variables/svg.dart';

class ProductListByLvl extends StatefulWidget {
  const ProductListByLvl({super.key});

  @override
  _ProductListByLvlState createState() => _ProductListByLvlState();
}

class _ProductListByLvlState extends State<ProductListByLvl> {
  final Map<String, String?> _selectedItemByOrder = {};
  @override
  Widget build(BuildContext context) {
    final locatorProvider = Provider.of<LocatorProvider>(context);
    final ordersByOrderNumber = locatorProvider.ordersByOrderNumber;

    return Scaffold(
      body: Stack(
        children: [
          const BackgroundImage(),
          Positioned.fill(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 30.0, left: 20, right: 20, bottom: 70),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: ordersByOrderNumber.entries.map((entry) {
                    final orderNumber = entry.key;
                    final items = entry.value;
                    _selectedItemByOrder.putIfAbsent(orderNumber, () => null);

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        // Divider between orders
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 12.0),
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          decoration: BoxDecoration(
                            color: Colors.grey[
                                300], // Light grey background for separation
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Center(
                            child: Text(
                              'Order: $orderNumber',
                              style: const TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black87,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6.0),
                          ),
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              // Table header
                              const Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 4.0, horizontal: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Item',
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      'Units',
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      'Localisation',
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const Divider(color: Colors.grey),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: items.map((item) {
                                  final row = item['loc'].substring(3, 5);
                                  final side = item['loc'].substring(5, 6);
                                  final column = item['loc'].substring(6, 8);
                                  final sides = item['loc'].substring(8, 9);
                                  final itemKey = item['item'];

                                  return GestureDetector(
                                    onTap: () async {
                                      final isSelected =
                                          _selectedItemByOrder[orderNumber] ==
                                              itemKey;

                                      if (isSelected) {
                                        // The item is currently selected and needs to be unhighlighted
                                        setState(() {
                                          _selectedItemByOrder[orderNumber] =
                                              null;
                                        });

                                        // Call unreserve function explicitly
                                        await locatorProvider
                                            .setItemUnreserved(item['id']);
                                      } else {
                                        // Unhighlight the previously selected item if there is one
                                        if (_selectedItemByOrder[orderNumber] !=
                                            null) {
                                          final previousItem = items.firstWhere(
                                            (i) =>
                                                i['item'] ==
                                                _selectedItemByOrder[
                                                    orderNumber],
                                          );

                                          // Call unreserve for the previously selected item
                                          await locatorProvider
                                              .setItemUnreserved(
                                                  previousItem['id']);
                                        }

                                        // Highlight and reserve the new item
                                        setState(() {
                                          _selectedItemByOrder[orderNumber] =
                                              itemKey;
                                        });

                                        // Call reserve function
                                        await locatorProvider
                                            .toggleItemReserved(
                                                orderNumber, item);
                                      }
                                    },
                                    child: Container(
                                      padding:
                                          const EdgeInsets.only(left: 15.0),
                                      decoration: BoxDecoration(
                                        color:
                                            _selectedItemByOrder[orderNumber] ==
                                                    itemKey
                                                ? const Color.fromARGB(
                                                    255, 253, 175, 214)
                                                : Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(3.0),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            item['item'],
                                            style: const TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            '${item['units']}',
                                            style: const TextStyle(
                                              fontSize: 16.0,
                                            ),
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                '$row$side$column$sides',
                                                style: const TextStyle(
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                              IconButton(
                                                icon: const Icon(
                                                  Icons.help_outline,
                                                  color: Colors.grey,
                                                  size: 20.0,
                                                ),
                                                onPressed: () {
                                                  showDialog(
                                                    context: context,
                                                    builder: (context) {
                                                      return AlertDialog(
                                                        title: const Text(
                                                            'Choisir votre option:'),
                                                        actions: <Widget>[
                                                          TextButton(
                                                            child: const Text(
                                                                'Barre code endomagé'),
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop(
                                                                      'Barre code endomagé');
                                                              // ==============================
                                                              // ADD LOGIC=====================
                                                              // ==============================
                                                            },
                                                          ),
                                                          TextButton(
                                                            child: const Text(
                                                                'Palette'),
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop(
                                                                      'Palette');
                                                              // ==============================
                                                              // ADD LOGIC=====================
                                                              // ==============================
                                                            },
                                                          ),
                                                        ],
                                                      );
                                                    },
                                                  );
                                                },
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 16.0,
            left: 16.0,
            child: ElevatedButton(
              onPressed: () async {
                // Iterate through the selected items to unreserve them if needed
                for (var entry in _selectedItemByOrder.entries) {
                  if (entry.value != null) {
                    final itemId = ordersByOrderNumber.values
                        .expand((items) => items)
                        .firstWhere(
                            (item) => item['item'] == entry.value)['id'];

                    // Unreserve the highlighted item
                    await locatorProvider.setItemUnreserved(itemId);
                  }
                }
                // Navigate back
                Navigator.pushReplacementNamed(context, '/getProduct');
              },
              style: ButtonStyle(
                backgroundColor: WidgetStateProperty.all<Color>(
                    const Color.fromARGB(255, 66, 59, 59)),
              ),
              child: ColorFiltered(
                colorFilter:
                    const ColorFilter.mode(Colors.white, BlendMode.srcIn),
                child: SvgPicture.string(backIconString),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BackgroundImage extends StatelessWidget {
  const BackgroundImage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Positioned.fill(
      child: DecoratedBox(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background_hp-red.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
